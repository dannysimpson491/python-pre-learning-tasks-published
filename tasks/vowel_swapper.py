def vowel_swapper(string):
    # ==============
    # Your code here
    
    new_string = ""
    
    for letter in string:
        if letter.casefold() == "a":
            letter = "4"
            new_string += letter
        elif letter.casefold() == "e":
            letter = "3"
            new_string += letter
        elif letter.casefold() == "i":
            letter = "!"
            new_string += letter
        elif letter == "o":
            letter = "ooo"
            new_string += letter
        elif letter == "O":
            letter = "000"
            new_string += letter
        elif letter.casefold() == "u":
            letter = "|_|"
            new_string += letter
        else:
            new_string += letter
    
    return new_string
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console